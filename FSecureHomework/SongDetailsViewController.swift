//
//  SongDetailsViewController.swift
//  FSecureHomework
//
//  Created by Muhammad Haris on 7/4/16.
//  Copyright © 2016 FSecure. All rights reserved.
//

import Foundation
import UIKit

class SongDetailsViewController: UIViewController {
    // MARK: Outlets
    @IBOutlet var songPosterImageView : UIImageView!
    @IBOutlet var songTitle: UILabel!
    @IBOutlet var songBandName: UILabel!
    @IBOutlet var songDescription: UITextView!
    // MARK: Variables
    var song : Song!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Updates view
        songTitle.text = song.title
        songBandName.text = song.bandName
        
        if let posterImage = song.poster {
           songPosterImageView.image =  posterImage
        }
        if let description = song.information {
            songDescription.text = description
        }
    }
}
