//
//  ViewController.swift
//  FSecureHomework
//
//  Created by Muhammad Haris on 7/3/16.
//  Copyright © 2016 FSecure. All rights reserved.
//

/**
 
 This class is meant for showing songs in a tableView
 
 */

import UIKit

class SongsViewController: UITableViewController {
    
    // MARK: Outlets
    @IBOutlet var activityIndicator : UIActivityIndicatorView!
    // MARK: Variables
    private var tableViewConfiguration: TableViewConfiguration?
    private var selectedSong : Song?
    let segueID = "SongDetailsViewController"
    let songTableViewCellID = "SongTableViewCell"
    let defaultSongTableViewCellHeight = CGFloat(50)
    
    // MARK: ViewController overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Songs"
        if self.tabBarController!.tabBar.selectedItem!.tag == kLocalSongsTabTag {
            populateSongsFromLocalDevice({ (songs) in
                self.updateUIWithData(songs)
            })
        }
        else if self.tabBarController!.tabBar.selectedItem!.tag == kRemoteSongsTabTag {
           populateSongsFromWeb(kSongsApiUrl, completion: { (songs) in
                self.updateUIWithData(songs)
           })
        }
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        selectedSong = nil
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == segueID) {
            let songDetailsView = segue.destinationViewController as! SongDetailsViewController
            songDetailsView.song = selectedSong
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     Loads the Songs from Network
     - parameter apiUrl: the url for which the data is to be loaded
     - parameter completion: returns songs array when completed
     - returns: no value
     */
    func populateSongsFromWeb (apiUrl:String, completion: (songs: [Song]) -> Void) {
        activityIndicator.startAnimating()
        guard let url = NSURL(string: apiUrl) else {
            let error = NSError(domain: kInValidUrl, code: kInValidUrlCode, userInfo: nil)
            showError(error)
            completion(songs: [])
            return
        };
        
        ModelManager.fetchSongsFromURL(url) { (songs, error) in
            if error != nil || songs == nil {
                self.handleErrorORoData(error, songs: songs)
                completion(songs: [])
            }
            guard let songsFetched = songs else {
                completion(songs: [])
                return
            }
            let updatedSongs = ModelManager.updateSongsCount(songsFetched)
            completion(songs: updatedSongs)
        }
    }
    
    /**
     Loads the Songs from Device
     - parameter completion: returns songs array when completed
     - returns: no value
     */
    func populateSongsFromLocalDevice(completion: (songs: [Song]) -> Void) {
        
        guard let filePath = String.filePathForJsonWithName("localTracks") else {
            let error = NSError(domain: kInValidFilePath, code: kInValidFilePathCode, userInfo: nil)
            showError(error)
            completion(songs: [])
            return
        }
        
        ModelManager.fetchSongsFromFileAtPath(filePath) { (songs, error) in
            if error != nil || songs == nil {
                self.handleErrorORoData(error, songs: songs)
            }
            guard let songsFetched = songs else {
                return
            }
            let songs = assignLocalImagesToSongs(songsFetched)
            let updatedSongs = ModelManager.updateSongsCount(songs)
            completion(songs: updatedSongs)
        }
    }
    
    /**
     Handle Error when the request is completed
     - parameter error: error in the response if any
     - parameter songs: returns songs array when request iss completed
     - returns: no value
     */
    func handleErrorORoData(error: NSError?, songs: [Song]?) {
        if let errorGenerated = error {
            self.showError(errorGenerated)
            return
        }
        guard songs != nil else {
            let error = NSError(domain: kNoSongsAvailable, code: kNoSongsAvailableCode, userInfo: nil)
            self.showError(error)
            return
        }
    }
    
    // MARK: UITableViewDataSource
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        guard let tableViewConfig = tableViewConfiguration else {return 0}
        return tableViewConfig.numberOfSections
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let tableViewConfig = tableViewConfiguration else {return 0}
        return tableViewConfig.numberOfRowsForSection(section)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let songCell = tableView.dequeueReusableCellWithIdentifier(songTableViewCellID) as! SongTableViewCell
        guard let tableViewConfig = tableViewConfiguration else {return songCell}
        return tableViewConfig.cellForRowAtIndexPath(songCell, indexPath: indexPath)!
    }
    
    // MARK: UITableViewDelegate
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        guard let tableViewConfig = tableViewConfiguration else {return defaultSongTableViewCellHeight}
        return tableViewConfig.rowHeight
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        guard let tableViewConfig = tableViewConfiguration else {return}
        guard let song = tableViewConfig.songAtIndexPath(indexPath) else {
            return
        }
        selectedSong = song
        performSegueWithIdentifier(segueID, sender: tableView.cellForRowAtIndexPath(indexPath))
    }
    
    // MARK: Error Handling
    /**
    shows the error on UI
     - parameter error: error to be shown on the UI
     - returns: No value.
     */
    func showError(error: NSError) {
        dispatch_async(dispatch_get_main_queue()) {
            self.activityIndicator.stopAnimating()
            let message = error.localizedDescription
            let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    /**
     updates UI and reload table after data fetch
     - parameter songs: songs for the tableView
     - returns: No value.
     */
    func updateUIWithData(songs: [Song]) {
        self.activityIndicator.stopAnimating()
        self.tableViewConfiguration = TableViewConfiguration(songsArray: songs)
        dispatch_async(dispatch_get_main_queue()) {
            self.tableView.reloadData()
        }
    }
    
}

