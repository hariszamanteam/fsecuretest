//
//  StringExtensions.swift
//  FSecureHomework
//
//  Created by Muhammad Haris on 7/5/16.
//  Copyright © 2016 FSecure. All rights reserved.
//

/** 
 Extension for generating string with particular length
 */
import Foundation
extension String {
    /**
     Generate alphanumeric string of given length
     - parameter length: length for which random string is needed to be generated
     - returns: random string of given kength.
     */
    static func randomStringWithLength(length: Int) -> String {
        
        let charactersToChoose = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 "
        let charactersCount = UInt32(charactersToChoose.characters.count)
        var result = ""
        
        for _ in (0..<length) {
            let index = Int(arc4random_uniform(charactersCount))
            let newCharacter = charactersToChoose[charactersToChoose.startIndex.advancedBy(index)]
            result += String(newCharacter)
        }
        
        return result
    }
    
    /**
     Gives filePath for a particular json file
     - parameter name: name of the file
     - returns: path of the input file
     */
    static func filePathForJsonWithName(name: String) -> String? {
        guard let filePath = NSBundle.mainBundle().pathForResource(name, ofType: "json") else {
            return nil
        }
        return filePath
    }
}
