//
//  SongViewControllerHelper.swift
//  FSecureHomework
//
//  Created by Muhammad Haris on 7/7/16.
//  Copyright © 2016 FSecure. All rights reserved.
//

/**
 
 This class is Helper methods for the SongViewController.swift
 
 */
import Foundation
import UIKit

// MARK: Helper
/**
    assign locally present images to songs
    - parameter songs: array of songs for which images are needed to be assigned
    - returns: array of songs which have local images
 */
func assignLocalImagesToSongs(songs: [Song]) -> [Song] {
    var localImagesArray: [UIImage] = []
    for i in 1...10 {
        guard let image = UIImage(named: "image\(i).jpg") else {
            continue
        }
        localImagesArray.append(image)
    }
    let imagesCount = UInt32(localImagesArray.count)
    
    for song in songs {
        let index = Int(arc4random_uniform(imagesCount))
        let randomImage = localImagesArray[localImagesArray.startIndex.advancedBy(index)]
        song.poster = randomImage
    }
    return songs
}

/**
 Struct for providing and validating data to tableView
 */
struct TableViewConfiguration {
    
    init(songsArray: [Song]) {songs = songsArray }
    //MARK: Variables
    private var songs: [Song] = []
    var numberOfSections: Int  { return 1 }
    var rowHeight: CGFloat { return  CGFloat(55) }
    
    /**
     tells how many rows tableView should have for a section
     - parameter section: section of the table view
     - returns: Int of how many rows a section can have
     */
    func numberOfRowsForSection(section: Int) -> Int {
        guard _isValidSection(section) else { return 0 }
        return songs.count
    }
    
    /**
     tells SongTableViewCell to update data from Songs
     - parameter cell: cell to be updated
     - returns: updated cell with song and image
     */
    func cellForRowAtIndexPath(cell:SongTableViewCell, indexPath: NSIndexPath) -> SongTableViewCell? {
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        guard let (_, row) = validateIndexPath(indexPath) else {
            return nil
        }
        let song = songs[row]
        cell.updateWithSong(song)
        cell.picture.image = song._cacheIconForSong({ (poster) in
            dispatch_async(dispatch_get_main_queue()) {
                cell.picture.image = poster
                cell.picture.setNeedsLayout()
            }
        })
        return cell
    }
    
    /**
     Gives song at particular row and section
     - parameter indexPath: indexPath for which song is needed
     - returns: song at input indexPath or nil
     */
    func songAtIndexPath(indexPath: NSIndexPath) -> Song? {
        guard let (_, row) = validateIndexPath(indexPath) else { return nil }
        return songs[row]
    }
    
    /**
     Check if section is valid
     - parameter section: section to be validated
     - returns: true if section is valid otherwise false
     */
    private func _isValidSection(section: Int) -> Bool { return section < numberOfSections }
    
    /**
     Check if row is valid
     - parameter row: row to be validated
     - parameter section: section of the row 
     - returns: true if section is valid otherwise false
     */
    private func _isValidRow(row: Int, section: Int) -> Bool { return row < numberOfRowsForSection(section) }
  
    /**
     Check if indexPath is valid
     - parameter indexPath: indexPath to be validated
     - returns: (section, row) if indexPathis valid otherwise nil
     */
    private func validateIndexPath(indexPath: NSIndexPath) -> (section: Int, row: Int)? {
        guard _isValidSection(indexPath.section) && _isValidRow(indexPath.row, section: indexPath.section) else { return nil }
        return (indexPath.section, indexPath.row)
    }
}