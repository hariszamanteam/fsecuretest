//
//  Song.swift
//  FSecureHomework
//
//  Created by Muhammad Haris on 7/3/16.
//  Copyright © 2016 FSecure. All rights reserved.
//

/**
 
Class for ModelObject of Song
 
 */

import Foundation
import UIKit

class Song: NSObject {
    // MARK: Variables
    var title: String!
    var bandName: String!
    var poster: UIImage?
    var posterUrl : NSURL?
    var information: String?
    // MARK: Constants
    let kTrackTitleKey = "track_title"
    let kTrackImageFileKey = "track_image_file"
    let kArtistNameKey = "artist_name"
    
    /**
     updates song from the json
     - parameter json: json of a song
     - returns: no value
     */
    func updateFromJson(json: [String: AnyObject]) {
        if let songTitle = json[kTrackTitleKey] as? String {
            self.title = songTitle
        } else {
            self.title = ""
        }
        if let songImageFileUrl = json[kTrackImageFileKey] as? String {
            self.posterUrl = NSURL(string: songImageFileUrl)
        } else {
            self.posterUrl = nil
        }
        if let bandName = json[kArtistNameKey] as? String {
            self.bandName = bandName
        } else {
            self.bandName = ""
        }
        self.poster = nil
        self.information = String.randomStringWithLength(200)
    }
    
    // MARK : Helper for downloading Song Image
    /**
     updates image of song from network
     - parameter completion: returns Image on request completion or nil is image load fails
     - returns: no value
     */
    func _cacheIconForSong(completion: ImageLoadCompletion) -> UIImage? {
        guard let posterUrl = self.posterUrl else {
            return nil
        }
        guard let cachePoster = self.poster else {
            // load it for server
            ModelManager.fetchImageFromURL(posterUrl, completion: { (poster, error) in
                if error != nil || poster == nil {return}
                self.poster = poster
                completion(poster: poster)
            })
            return nil
        }
        return cachePoster
    }
    

}