//
//  JsonParser.swift
//  FSecureHomework
//
//  Created by Muhammad Haris on 7/7/16.
//  Copyright © 2016 FSecure. All rights reserved.
//
/**
 
 This class is meant for parse json and extracting songs from the json
 
 */
import Foundation

class JsonParser {
    static let sharedInstance = JsonParser()
    let kDataSetKey = "dataset"
    // MARK: Json Parsing
    /**
     Parses the json to extract songs from it
     - parameter json: json to be parsed
     - returns: songs array extracted from json.
     */
    func parseSongsFromJson(json: [String: AnyObject]) -> [Song] {
        
        guard let tracks = json[kDataSetKey] as? [AnyObject] else {
            return []
        }
        
        var songs: [Song] = []
        for track in tracks {
            guard let trackJson = track as? [String: AnyObject] else {
                return songs
            }
            let song = Song()
            song.updateFromJson(trackJson)
            songs.append(song)
        }
        return songs
    }
}