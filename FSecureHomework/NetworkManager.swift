//
//  NetworkManager.swift
//  FSecureHomework
//
//  Created by Muhammad Haris on 7/7/16.
//  Copyright © 2016 FSecure. All rights reserved.
//

/**
 
 This class is meant for handling network requests
 
 */
import Foundation

//MARK: Block Definitions
/**
 Initial request completion block
 - parameter data: data on request completion
 - parameter response: response on request completion
 - parameter error: error on request completion
  - returns: No value.
 */
public typealias RequestCompletion = ( data: NSData?, response:  NSURLResponse?, error: NSError? ) -> Void
/**
 Final request completion block
 - parameter json: json from request completion
 - parameter error: error on request completion
 - returns: No value.
 */
public typealias FinalRequestCompletion = ( json: [String:AnyObject]?, error: NSError? ) -> Void

public class NetworkManager {
    // used for session of the request
    public let session: NSURLSession
    
    // initialization of singleton
    public static let sharedInstance: NetworkManager = {
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        return NetworkManager(configuration: configuration)
    }()
    public init (configuration: NSURLSessionConfiguration) {
        session = NSURLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
    }
    
    /**
     Trigger request execution
     - parameter request: request to be executed
     - parameter completion: return data, response, error on request completion
     - returns: No value.
     */
    func executeRequest (request: NSMutableURLRequest, completion: RequestCompletion) {
        let dataTask = session.dataTaskWithRequest(request) { (data, response, error) in
            completion(data: data, response: response, error: error)
        }
        dataTask.resume()
    }
    
    /**
     Performs high level request reponse
     - parameter dataInResponse: data on request completion
     - parameter response: response on request completion
     - parameter error: error on request completion
     - parameter completion: return data, response, error on request completion
     - returns: No value.
     */
    class func processRequestResponse (dataInResponse: NSData?, response: NSURLResponse?, error: NSError? , completion: FinalRequestCompletion) {
        
        guard error == nil else {
            completion (json: nil, error: error)
            return
        }
        
        guard let httpResponse = response as? NSHTTPURLResponse else {
            if error == nil {
                 let errorGenerated = NSError(domain: kInValidUrlResponse, code: kInValidUrlResponseCode, userInfo: nil)
                  completion (json: nil,  error: errorGenerated)
                return
            }
            completion (json: nil,  error: error)
            return
        }
        
        guard httpResponse != 200 else {
            if error == nil {
                let errorGenerated = NSError(domain: kInValidUrlResponse, code: kInValidUrlResponseCode, userInfo: nil)
                completion (json: nil,  error: errorGenerated)
                return
            }
            completion (json: nil, error: error)
            return;
        }
        ModelManager.serializeData(dataInResponse, completion: completion)
    }
    

    deinit {
        session.invalidateAndCancel()
    }
}