//
//  SongTableViewCell.swift
//  FSecureHomework
//
//  Created by Muhammad Haris on 7/3/16.
//  Copyright © 2016 FSecure. All rights reserved.
//

/**
 
 This class is the custom song rcell of tableView
 
 */
import Foundation
import UIKit
/**
   block returns image when loaded from network
 - returns: No value.
 */
public typealias ImageLoadCompletion = (poster: UIImage?) -> Void

class SongTableViewCell: UITableViewCell {
    //MARK: Outlets
    @IBOutlet var picture: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var bandName: UILabel!
    
    /**
     updates the data of songcell
     - parameter song: song fir which cell needs update
     - returns: No value.
     */
    func updateWithSong(song: Song) {
        defer {
            title.text = song.title
            bandName.text = song.bandName
        }
        guard (song.poster != nil) else {
         return
        }
        picture.image = song.poster
    }
}