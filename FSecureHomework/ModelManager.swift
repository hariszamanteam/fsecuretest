//
//  ModelManager.swift
//  FSecureHomework
//
//  Created by Muhammad Haris on 7/7/16.
//  Copyright © 2016 FSecure. All rights reserved.
//


/**
 
 This class is meant for communication between view controllers and network related tasks.
 It also provide some helper methods to update data for view controllers
 
 */

import Foundation
import UIKit

typealias SongsDataFetchCompletion = ( songs: [Song]?, error: NSError? ) -> Void
typealias SongsImageFetchCompletion = ( poster: UIImage?, error: NSError? ) -> Void
class ModelManager {
    
    // MARK: Networking
    /**
     Fetches songs for a particular url
     - parameter url: The input url from which the data is o be loaded
     - parameter completion: returns fetched songs and error if any
     - returns: No value.
     */
    class func fetchSongsFromURL(url : NSURL, completion: SongsDataFetchCompletion) {
        let request = NSMutableURLRequest(URL: url)
        executeRequest(request, completion: completion)
        
    }
    
    /**
     Execute the request
     - parameter request: The request that is to be executed
     - parameter completion: returns fetched songs and error if any
     - returns: No value.
     */
    class func executeRequest(request: NSMutableURLRequest, completion: SongsDataFetchCompletion){
        NetworkManager.sharedInstance.executeRequest(request) { (data, response, error) in
            processResponse(data, response: response, error: error, completion: completion)
        }
    }
    
    /**
     Fetch an image for a song from a url
     - parameter url: The url from which the imaged is to be fetched
     - parameter completion: returns fetched songs and error if any
     - returns: No value.
     */
    class func fetchImageFromURL(url : NSURL, completion: SongsImageFetchCompletion) {
        let request = NSMutableURLRequest(URL: url)
        NetworkManager.sharedInstance.executeRequest(request) { (data, response, error) in
            guard let data = data where error == nil else {
                completion(poster: nil, error: error)
                return
            }
            debugPrint("Downloading image completed for URL: \(url)")
            guard let poster = UIImage(data: data) else {
                completion(poster: nil, error: error)
                return
            }
            completion(poster: poster, error: error)
        }
    }
    
    /**
     Process response of a network request
     - parameter data: output data from request
     - parameter response: url response of request
     - parameter error: error in the request if any
     - parameter completion: returns fetched songs and error if any
     - returns: No value.
     */
    class func processResponse(data: NSData?, response:  NSURLResponse?, error: NSError?, completion: SongsDataFetchCompletion){
        NetworkManager.processRequestResponse(data, response: response, error: error) { (json, error) in
            if error != nil {
                completion(songs: nil, error: error)
                return
            }
            processJson(json, completion: completion)
        }
    }
    
    /**
     Process json returned from the request
     - parameter json: json to be processed
     - parameter completion: returns fetched songs and error if any
     - returns: No value.
     */
    class func processJson(json: [String:AnyObject]?, completion: SongsDataFetchCompletion) {
        
        guard let jsonToParse = json else {
            let errorToReturn = NSError(domain: kJsonIsNil, code: kJsonIsNilCode, userInfo: nil)
            completion(songs: nil, error: errorToReturn)
            return
        }
        let songs = JsonParser.sharedInstance.parseSongsFromJson(jsonToParse)
        completion(songs: songs, error: nil)
    }
    
    // MARK: Reading Songs Locally
    /**
     Fetches songs from a local json
     - parameter filePath: path of the local json file
     - parameter completion: returns fetched songs and error if any
     - returns: No value.
     */
    class func fetchSongsFromFileAtPath(filePath : String, completion: SongsDataFetchCompletion) {
        if !IsFilePresentWithPath(filePath, completion: completion) {
            return
        }
        
        let data = NSData(contentsOfFile: filePath)
        serializeData(data) { (json, error) in
            if error != nil {
                completion(songs: nil, error: error)
                return
            }
            processJson(json, completion: completion)
        }
    }
    
    /**
     checks if the file is exist at path
     - parameter filePath: path at which the file is present or not
     - returns: true if file exists otherwise false.
     */
    class func fileExistsAtPath(filePath: String?) -> Bool {
        if let filePathNotNil = filePath {
            let fileManager = NSFileManager.defaultManager()
            return fileManager.fileExistsAtPath(filePathNotNil)
        } else {
            return false
        }
    }
    
    /**
     checks if the file is exist at path with completion
     - parameter filePath: path at which the file is present or not
     - parameter completion: returns songs or error if any after checking the file is present or not
     - returns: no value
     */
    class func IsFilePresentWithPath(filePath: String?, completion: SongsDataFetchCompletion) -> Bool {
        let fileExistsAtPath = ModelManager.fileExistsAtPath(filePath)
        if  !fileExistsAtPath {
            let error = NSError(domain: kNoFileFound, code: kNoFileFoundCode, userInfo: nil)
            completion(songs: [], error: error)
            return false
        }
        return true
    }
    
    // MARK: Updating Songs Count
    /**
     Helper method json to return same number of programs as stated in Constants.swift file
     - parameter songs: input of songs array which may be < kNumberOfSongsToBeGenerated
     - returns: songs which are = kNumberOfSongsToBeGenerated.
     */
    class func updateSongsCount(songs : [Song]) -> [Song] {
        var updatedSongs: [Song] = songs
        let numberOfDummySongs = kNumberOfSongsToBeGenerated - songs.count
        
        if numberOfDummySongs <= 0 {
            return songs
        }
        let songsCount = UInt32(songs.count)
        for _ in 1...numberOfDummySongs {
            let index = Int(arc4random_uniform(songsCount))
            let randomSong = songs[songs.startIndex.advancedBy(index)]
            updatedSongs.append(randomSong)
        }
        return updatedSongs
    }
    
    // MARK: Data Serializing
    /**
     Method to serialize data from network request response
     - parameter data: data from network request
     - parameter completion: returns json and error if any
     - returns: songs which are = kNumberOfSongsToBeGenerated.
     */
    class func serializeData(dataIn: NSData?, completion: FinalRequestCompletion) {
        
        guard let data = dataIn where data.length > 0 else {
            completion (json: nil, error: nil)
            return;
        }
        do {
            guard let dictionary = try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String: AnyObject] else {
                completion (json: nil, error: nil)
                return;
            }
            completion(json: dictionary,  error: nil)
            
        } catch let jsonError as NSError {
            guard let responseString =  NSString(data: data, encoding:NSUTF8StringEncoding) where !responseString.isEqualToString("") else {
                completion (json: nil, error: jsonError)
                return;
            }
            let dictionaryFromString = ["error": responseString]
            completion (json: dictionaryFromString, error: jsonError)
        }
    }
    
}