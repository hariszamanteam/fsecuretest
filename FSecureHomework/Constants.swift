//
//  Constants.swift
//  FSecureHomework
//
//  Created by Muhammad Haris on 7/7/16.
//  Copyright © 2016 FSecure. All rights reserved.
//

import Foundation

// MARK: Global Constants
let kLocalSongsTabTag = 999
let kRemoteSongsTabTag = 000
let kNumberOfSongsToBeGenerated = 100
let kSongsApiUrl = "https://freemusicarchive.org/api/get/tracks.json?api_key=60BLHNQCAOUFPIBZ&curator_handle=wfmu"

// MARK: Errors Strings
let kNoFileFound = "No File Found"
let kNoFileFoundCode = 1
let kJsonIsNil = "Json is nil"
let kJsonIsNilCode = 2
let kInValidUrl = "The Url for loading songs is invalid"
let kInValidUrlCode = 3
let kInValidFilePath = "File Path for local json is invalid"
let kInValidFilePathCode = 4
let kNoSongsAvailable = "No Songs are available"
let kNoSongsAvailableCode = 5
let kInValidUrlResponse = "The Url Response is Invalid"
let kInValidUrlResponseCode = 6