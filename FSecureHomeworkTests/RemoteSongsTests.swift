//
//  RemoteSongsTests.swift
//  FSecureHomework
//
//  Created by Muhammad Haris on 7/8/16.
//  Copyright © 2016 FSecure. All rights reserved.
//

import XCTest
@testable import FSecureHomework

class RemoteSongsTests: FSecureHomeworkTests {
    
    override func setUp() {
        super.setUp()
    }
    
    /**
     Test remote songs population
     */
    func testSongsPopulation() {
        let expectation = expectationWithDescription("test song populate")
        var songsFetched : [Song] = []
        songsViewController.populateSongsFromWeb(kSongsApiUrl) { (songs) in
            songsFetched = songs
            XCTAssertEqual(songsFetched.count, kNumberOfSongsToBeGenerated)
            expectation.fulfill()
        }
        waitForExpectationsWithTimeout(self.timeout, handler: nil)
    }
    
    /**
     Test remote songs loading from a valid url
     */
    func testFetchingSongsFromValidUrl() {
        let expectation = expectationWithDescription("test song populate")
        let url = NSURL(string: "https://www.abc.com")
        ModelManager.fetchSongsFromURL(url!) { (songs, error) in
            XCTAssertNil(songs)
            expectation.fulfill()
        }
        waitForExpectationsWithTimeout(self.timeout, handler: nil)
    }
}
