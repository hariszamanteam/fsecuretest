//
//  ModelManagerTests.swift
//  FSecureHomework
//
//  Created by Muhammad Haris on 7/8/16.
//  Copyright © 2016 FSecure. All rights reserved.
//

import XCTest
@testable import FSecureHomework

class ModelManagerTests: FSecureHomeworkTests {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    /**
     Test response with a given error
     */
    func testProcessResponseWithError() {
        let error  = NSError (domain: "test", code: 1, userInfo: [:])
        var responseError: NSError?
        ModelManager.processResponse(NSData(), response: NSURLResponse(), error: error) { (songs, error) in
            responseError = error
        }
        XCTAssertEqual(error, responseError, "error sent should be error returned")
    }
    
    /**
     Test proessing of json which is nil
     */
    func testProcessJsonWithNil() {
        ModelManager.processJson(nil) { (songs, error) in
            kJsonIsNilCode
            XCTAssertEqual(error?.code, kJsonIsNilCode, "error code should match")
        }
    }
    
    /**
     Test image loading from a valid url
     */
    func testImageLoadingForAValidUrl() {
        let urlString = "https://placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150"
        let url = NSURL(string: urlString)
        let expectation = expectationWithDescription("test image loading")
        
        ModelManager.fetchImageFromURL(url!) { (poster, error) in
            XCTAssertNotNil(poster)
            expectation.fulfill()
        }
        waitForExpectationsWithTimeout(self.timeout, handler: nil)
    }
    
    /**
     Test image loading from a an inavalid url
     */
    func testImageLoadingForAnInValidUrl() {
        let urlString = "https://placeholdit.imgix.net/~text"
        let url = NSURL(string: urlString)
        let expectation = expectationWithDescription("test image loading")
        
        ModelManager.fetchImageFromURL(url!) { (poster, error) in
            XCTAssertNil(poster)
            expectation.fulfill()
        }
        waitForExpectationsWithTimeout(self.timeout, handler: nil)
    }
    
    /**
     Test if invalid file path logic is working fine or not
     */
    func testFileAtInValidPath() {
       let filePath = String.filePathForJsonWithName("localInValidTracks")
        XCTAssertNil(filePath)
        let filePresent = ModelManager.IsFilePresentWithPath(filePath) { (songs, error) in
                        XCTAssertNotNil(error)
                        XCTAssertEqual(error?.code, kNoFileFoundCode, "error code should match")
        }
        XCTAssertFalse(filePresent)
    }
    
    /**
     Test empty data given for serializing
     */
    func testDataWithEmptyString() {
        let stringData = ""
        let data = stringData.dataUsingEncoding(NSUTF8StringEncoding)
        let expectation = expectationWithDescription("Test invalid response)")
        ModelManager.serializeData(data!) { (json, error) in
            XCTAssertNil(json, "json should be nil if string is empty")
            expectation.fulfill()
        }
        waitForExpectationsWithTimeout(self.timeout, handler: nil)
    }
    
    /**
     Test an invalid json given serializing
     */
    func testInValidJson() {
        let stringData = "String converted to Data"
        let data = stringData.dataUsingEncoding(NSUTF8StringEncoding)
        let expectation = expectationWithDescription("Test invalid json response)")
        ModelManager.processResponse(data, response: NSHTTPURLResponse(), error: nil) { (songs, error) in
            XCTAssertNotNil(error)
            expectation.fulfill()
        }
        waitForExpectationsWithTimeout(self.timeout, handler: nil)
    }
    
    
}
