//
//  FSecureHomeworkTests.swift
//  FSecureHomeworkTests
//
//  Created by Muhammad Haris on 7/3/16.
//  Copyright © 2016 FSecure. All rights reserved.
//

import XCTest
@testable import FSecureHomework

class FSecureHomeworkTests: XCTestCase {
    //MARK: Global variable for Tests
    let timeout: NSTimeInterval = 30.0
    var kRequestTime =  NSTimeInterval(0.1)
    var kResponseTime = NSTimeInterval(0.5)
    var songsViewController : SongsViewController!
    var tableViewConfiguration : TableViewConfiguration!
    var songs : [Song] = []
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        songsViewController = storyboard.instantiateViewControllerWithIdentifier("SongsViewController") as! SongsViewController
        songsViewController.loadView()
        for i in 1...kNumberOfSongsToBeGenerated {
            let song = Song()
            song.title = "title\(i)"
            song.bandName = "bandName\(i)"
            songs.append(song)
        }
        tableViewConfiguration = TableViewConfiguration(songsArray: songs)
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    /**
     Test number of sections for tableview are correct
     */
    func testNumberOfSections() {
        XCTAssertEqual(tableViewConfiguration.numberOfSections, 1)
    }
    
    /**
     Test number of rows in a tableview is equal to songs fetched
     */
    func testNumberOfRowsInValidSection() {
        let rowsCount = tableViewConfiguration.numberOfRowsForSection(0)
        XCTAssertEqual(rowsCount, songs.count)
        
    }
    /**
     Test number of rows in a tableview invalid setion
     */
    func testNumberOfRowsInInValidSection() {
        let rowsCount = tableViewConfiguration.numberOfRowsForSection(1)
        XCTAssertEqual(rowsCount, 0)
        
    }
    
    /**
     Validates Indexpath of a tavbleview
     */
    func testSongAtValidateIndexPath() {
        let index = 5
        let validIndexPath = NSIndexPath(forRow: index, inSection: 0)
        let songFromTable = tableViewConfiguration.songAtIndexPath(validIndexPath)
        XCTAssertEqual(songFromTable, songs[index])
        
    }
    
    /**
       Test song at invalid index would be nil
     */
    func testSongAtInvalidIndex() {
        let invalidIndexPath = NSIndexPath(forRow: kNumberOfSongsToBeGenerated+1, inSection: 0)
        let song = tableViewConfiguration.songAtIndexPath(invalidIndexPath)
        XCTAssertNil(song)
        
    }
    
    /**
     Test song title at index is equal to cell title at the same index
     */
    func testSongCellAtValidIndexPath(){
        let index = 5
        let validIndexPath = NSIndexPath(forRow: index, inSection: 0)
        let cell = songsViewController.tableView.dequeueReusableCellWithIdentifier("SongTableViewCell")
            as! SongTableViewCell
        let songCell = tableViewConfiguration.cellForRowAtIndexPath(cell, indexPath: validIndexPath)
        XCTAssertEqual(songCell?.title.text, songs[index].title)
        
    }
    /**
     Test cell should be nil invalid indexpath
     */
    func testSongCellAtInValidIndexPath(){
        let invalidIndexPath = NSIndexPath(forRow: kNumberOfSongsToBeGenerated+1, inSection: 0)
        let cell = SongTableViewCell()
        let songCell = tableViewConfiguration.cellForRowAtIndexPath(cell, indexPath: invalidIndexPath)
        XCTAssertNil(songCell)
        
    }
}


