//
//  LocalSongsTests.swift
//  FSecureHomework
//
//  Created by Muhammad Haris on 7/8/16.
//  Copyright © 2016 FSecure. All rights reserved.
//

import XCTest
@testable import FSecureHomework

class LocalSongsTests: FSecureHomeworkTests {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    /**
     Test local songs population
     */
    func testSongsPopulation() {
        let expectation = expectationWithDescription("test song populate")
        var songsFetched : [Song] = []
        songsViewController.populateSongsFromLocalDevice { (songs) in
            songsFetched = songs
            expectation.fulfill()
        }
        XCTAssertEqual(songsFetched.count, kNumberOfSongsToBeGenerated)
        waitForExpectationsWithTimeout(self.timeout, handler: nil)
    }
    
}
