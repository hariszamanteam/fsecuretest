//
//  NetworkManagerTests.swift
//  FSecureHomework
//
//  Created by Muhammad Haris on 7/8/16.
//  Copyright © 2016 FSecure. All rights reserved.
//

import XCTest
@testable import FSecureHomework
class NetworkManagerTests: FSecureHomeworkTests {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    /**
      Test for processing empty response for request
     */
    func testProcessEmptyResponse() {
        
        let response = NSURLResponse()
        NetworkManager.processRequestResponse(nil, response: response, error: nil) { (json, errorReturned) in
            XCTAssertNotNil(errorReturned)
            XCTAssertEqual(errorReturned?.code, kInValidUrlResponseCode)
        }
    }
    
}
